# SDA Final Project - Online Store

This "iTech Online Store" is a web application that has basically 2 main functionalities, that of an admin and a user. The system allows administration panel to add products and enable user registration and logging, as well as placing an order.