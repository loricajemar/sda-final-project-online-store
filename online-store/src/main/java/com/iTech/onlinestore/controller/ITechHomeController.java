package com.iTech.onlinestore.controller;

import com.iTech.onlinestore.entity.UserEntity;
import com.iTech.onlinestore.service.ITechHomeService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ITechHomeController {

    final ITechHomeService iTechHomeService;
    public ITechHomeController(ITechHomeService iTechHomeService) {
        this.iTechHomeService = iTechHomeService;
    }

    @GetMapping("/home")
    public String goToHome(final Model modelMap) {
        modelMap.addAttribute("home");
        return "index";
    }

    @GetMapping("/sign-in")
    public String showSignInPage(final Model modelMap) {
        modelMap.addAttribute("signInPage");
        return "sign-in";
    }

    @PostMapping("/sign-in")
    public String signIn(@ModelAttribute("signInPage") final UserEntity userEntity, Errors errors) {
        iTechHomeService.getUserByIdAndEmailAndPassword(
                userEntity.getId(), userEntity.getEmail(), userEntity.getPassword());
        if (errors.hasErrors()) {
            return "error";
        }
        return "redirect:/admin-page";

    }
}
