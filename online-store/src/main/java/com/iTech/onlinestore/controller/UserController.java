package com.iTech.onlinestore.controller;

import com.iTech.onlinestore.entity.UserEntity;
import com.iTech.onlinestore.model.User;
import com.iTech.onlinestore.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.SecureRandom;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    final UserService userService;
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/new")
    public String showUserPage(final ModelMap modelMap) {
        modelMap.addAttribute("user", new User());
        return "user-registration";
    }

    private SecureRandom secureRandom = new SecureRandom();

    @PostMapping("/new")
    @ResponseStatus(HttpStatus.CREATED)
    public String addUser(@Valid @ModelAttribute("user") final UserEntity newUser, Errors errors) {
        UserEntity user = new UserEntity();
        user.setId(Long.parseLong(String.valueOf(Math.abs(secureRandom.nextLong()))));
        user.setEmail(newUser.getEmail());
        user.setPassword(newUser.getPassword());
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getFirstName());
        user.setCity(newUser.getCity());
        user.setZipCode(newUser.getZipCode());
        user.setCountry(newUser.getCountry());
        user.setRole(newUser.getRole());
        user.setMessageChannelPreference(newUser.getMessageChannelPreference());
        userService.createUser(newUser);
        if (errors.hasErrors()) {
            return "error";
        }
        return "redirect:/welcome";
    }

    @GetMapping("/all")
    public List<UserEntity> retrieveAllUser() {
        return userService.getAllUser();
    }

    @GetMapping("/sign-in")
    public String signIn(User user) {
        return "user-page";
    }
}
