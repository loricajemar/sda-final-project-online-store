package com.iTech.onlinestore.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "category", schema = "online_store")
public class CategoryEntity {
    private long id;
    private String categoryName;
    private long parentCategoryId;
    private long childrenCategoryId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "category_name", nullable = false, length = 255)
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Basic
    @Column(name = "parent_category_id", nullable = false)
    public long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(long parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    @Basic
    @Column(name = "children_category_id", nullable = false)
    public long getChildrenCategoryId() {
        return childrenCategoryId;
    }

    public void setChildrenCategoryId(long childrenCategoryId) {
        this.childrenCategoryId = childrenCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryEntity that = (CategoryEntity) o;
        return id == that.id && parentCategoryId == that.parentCategoryId && childrenCategoryId == that.childrenCategoryId && Objects.equals(categoryName, that.categoryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryName, parentCategoryId, childrenCategoryId);
    }
}
