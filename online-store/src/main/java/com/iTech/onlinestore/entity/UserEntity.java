package com.iTech.onlinestore.entity;

import com.iTech.onlinestore.model.MessageChannelPreference;
import com.iTech.onlinestore.model.RoleName;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "online_store")
public class UserEntity {
    private long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String city;
    private int zipCode;
    private String country;
    private RoleName role;
    private MessageChannelPreference messageChannelPreference;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 255)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "city", nullable = false, length = 255)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "zip_code", nullable = false)
    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    @Basic
    @Column(name = "country", nullable = false, length = 255)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "role", nullable = false)
    public RoleName getRole() {
        return role;
    }

    public void setRole(RoleName role) {
        this.role = role;
    }

    @Basic
    @Column(name = "message_channel_preference", nullable = true)
    public MessageChannelPreference getMessageChannelPreference() {
        return messageChannelPreference;
    }

    public void setMessageChannelPreference(MessageChannelPreference messageChannelPreference) {
        this.messageChannelPreference = messageChannelPreference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return id == that.id && zipCode == that.zipCode && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName) && Objects.equals(city, that.city) && Objects.equals(country, that.country) && Objects.equals(role, that.role) && Objects.equals(messageChannelPreference, that.messageChannelPreference);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, firstName, lastName, city, zipCode, country, role, messageChannelPreference);
    }
}
