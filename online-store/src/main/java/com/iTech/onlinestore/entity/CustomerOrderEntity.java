package com.iTech.onlinestore.entity;

import com.iTech.onlinestore.model.Status;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "customer_order", schema = "online_store")
public class CustomerOrderEntity {
    private long id;
    private String userName;
    private float totalCost;
    private String deliveryAddress;
    private String userAddress;
    private LocalDateTime dateOfOrder;
    private Status status;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_name", nullable = false, length = 255)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "total_cost", nullable = true, precision = 2)
    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    @Basic
    @Column(name = "delivery_address", nullable = false, length = 255)
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    @Basic
    @Column(name = "user_address", nullable = false, length = 255)
    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    @Basic
    @Column(name = "date_of_order", nullable = false)
    public LocalDateTime getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(LocalDateTime dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerOrderEntity that = (CustomerOrderEntity) o;
        return id == that.id && Objects.equals(userName, that.userName) && Objects.equals(totalCost, that.totalCost) && Objects.equals(deliveryAddress, that.deliveryAddress) && Objects.equals(userAddress, that.userAddress) && Objects.equals(dateOfOrder, that.dateOfOrder) && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, totalCost, deliveryAddress, userAddress, dateOfOrder, status);
    }
}
