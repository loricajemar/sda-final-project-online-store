package com.iTech.onlinestore.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "order_line", schema = "online_store")
public class OrderLineEntity {
    private long id;
    private int numberOfProducts;
    private float productPrice;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "number_of_products", nullable = true)
    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    @Basic
    @Column(name = "product_price", nullable = true, precision = 2)
    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderLineEntity that = (OrderLineEntity) o;
        return id == that.id && Objects.equals(numberOfProducts, that.numberOfProducts) && Objects.equals(productPrice, that.productPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numberOfProducts, productPrice);
    }
}
