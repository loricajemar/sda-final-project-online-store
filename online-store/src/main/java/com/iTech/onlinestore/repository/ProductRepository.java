package com.iTech.onlinestore.repository;

import com.iTech.onlinestore.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<ProductEntity, Long> {
}
