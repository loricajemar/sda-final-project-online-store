package com.iTech.onlinestore.repository;

import com.iTech.onlinestore.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity getUserByIdAndAndEmailAndPassword(
            long id, String email, String password);
    @Query("select u from UserEntity u where u.email = :email and u.password = :password")
    List<UserEntity> getUserByIdAndEmailAAndPassword(
            @Param("email") String email, @Param("password") String password);

    @Query("select u from UserEntity u")
    List<UserEntity> getAllUser();
}
