package com.iTech.onlinestore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Validated
public class Product{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    private String description;

    @NotEmpty
    @OneToMany
    private Category category;

    @NotEmpty
    private float price;

    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @NotEmpty
    @OneToMany
    private Author author;

}
