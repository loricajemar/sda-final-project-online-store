package com.iTech.onlinestore.model;

public enum ProductType {
    DESKTOP,
    LAPTOP,
    PRINTER,
    MONITOR,
    SMART_PHONE,
    TABLET,
    SMART_WATCH,
    EAR_PHONE

}
