package com.iTech.onlinestore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty
    private String categoryName;

    @NotEmpty
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long parentCategoryId;

    @NotEmpty
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long childrenCategoryId;

}
