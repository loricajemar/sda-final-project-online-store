package com.iTech.onlinestore.model;

public enum Status {
    PACKED,
    SHIPPED,
    DELIVERED

}
