package com.iTech.onlinestore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class CustomerOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Please enter username/ email.")
    private String userName;

    private float totalCost;

    @NotEmpty(message = "Please enter shipping address.")
    private String deliveryAddress;

    @NotEmpty(message = "Please enter your current address.")
    private String userAddress;

    private LocalDateTime dateOfOrder;

    @OneToOne
    private OrderLine orderLine;

    @OneToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private Status status;

}
