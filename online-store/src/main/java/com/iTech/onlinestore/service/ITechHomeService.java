package com.iTech.onlinestore.service;

import com.iTech.onlinestore.entity.UserEntity;
import com.iTech.onlinestore.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class ITechHomeService {

    final UserRepository userRepository;
    public ITechHomeService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserEntity getUserByIdAndEmailAndPassword(
            final long id, final String email, final String password) {
        return userRepository.getUserByIdAndAndEmailAndPassword(id, email, password);
    }
}
