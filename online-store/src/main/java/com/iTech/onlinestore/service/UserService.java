package com.iTech.onlinestore.service;

import com.iTech.onlinestore.entity.UserEntity;
import com.iTech.onlinestore.repository.UserRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.util.List;

@Service
public class UserService {

    final UserRepository userRepository;
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private SecureRandom secureRandom = new SecureRandom();

    @Transactional
    public UserEntity createUser(final UserEntity newUser) {
        UserEntity user = new UserEntity();
        user.setId(Long.parseLong(String.valueOf(Math.abs(secureRandom.nextLong()))));
        user.setEmail(newUser.getEmail());
        user.setPassword(newUser.getPassword());
        user.setFirstName(newUser.getFirstName());
        user.setLastName(newUser.getLastName());
        user.setCity(newUser.getCity());
        user.setZipCode(newUser.getZipCode());
        user.setCountry(newUser.getCountry());
        user.setRole(newUser.getRole());
        user.setMessageChannelPreference(newUser.getMessageChannelPreference());
        userRepository.save(user);
        return user;
    }

    public List<UserEntity> getAllUser() {
        return userRepository.getAllUser();
    }
}
