package com.iTech.onlinestore.error;

public class DataNotFoundException extends RuntimeException{
    public DataNotFoundException(String exception) {
        super(exception);
    }

}
