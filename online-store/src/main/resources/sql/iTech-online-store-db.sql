create database online_store;

-- create table admin(
-- id int primary key auto_increment not null,
-- email varchar(255) not null,
-- password varchar(255) not null,
-- first_name varchar(255) not null,
-- last_name varchar(255) not null,
-- city varchar(255) not null,
-- zip_code int(5) not null,
-- country varchar(255) not null,
-- role varchar(30) default 'admin'
-- );

create table if not exists user(
id int primary key auto_increment not null,
email varchar(255) not null,
password varchar(255) not null,
first_name varchar(255) not null,
last_name varchar(255) not null,
city varchar(255) not null,
zip_code int(5) not null,
country varchar(255) not null,
role varchar(30) default 'user'
);

-- alter table admin modify column id bigint;

alter table user modify column id bigint;

select * from user;
show columns from user;
select * from customer_order;
show columns from customer_order;

create table if not exists product(
id bigint primary key not null auto_increment,
title varchar(255) not null,
description varchar(500) default 'Description not provided.',
category varchar(255) not null,
price decimal(9,2) not null,
product_type varchar(255) not null,
author varchar(255) not null,
constraint adminId_fk foreign key (id) references admin(id)
);

create table if not exists role(
id bigint primary key not null auto_increment,
role_name varchar(30) not null,
foreign key (id) references admin(id),
foreign key (id) references user(id)
);

-- alter table role drop foreign key role_ibfk_1;
-- alter table product drop foreign key adminId_fk;
-- drop table admin;
-- drop table role;

create table if not exists category(
id bigint primary key not null auto_increment,
category_name varchar(255) not null,
parent_category_id bigint not null unique,
children_category_id bigint not null unique,
constraint productId_fk foreign key (id) references product(id)
);

create table if not exists customer_order(
id bigint primary key not null auto_increment,
user_name varchar(255) not null,
total_cost decimal(9,2) default 0.00,
delivery_address varchar(255) not null,
user_address varchar(255) not null,
date_of_order timestamp not null,
status enum('packed', 'shipped', 'delivered')
);

create table if not exists order_line(
id bigint primary key not null auto_increment,
number_of_products int default 0,
product_price decimal(9,2) default 0.00,
foreign key (id) references product(id),
constraint customerOrderId_fk foreign key (id) references customer_order(id)
);

create table if not exists author(
id bigint primary key not null auto_increment,
first_name varchar(255) not null,
last_name varchar(255) not null,
foreign key (id) references product(id)
);

alter table customer_order add constraint userId_fk foreign key (id) references user(id);
alter table product add constraint userRole_fk foreign key (id) references user(id);

-- alter table admin modify column role int default 1;
-- alter table user modify column role int default 2;

alter table user add column message_channel_preference enum('standard mail', 'email');
alter table user modify column role enum('admin', 'user') not null;

alter table product modify column product_type enum(
'desktop', 
'laptop', 
'printer', 
'monitor', 
'smart phone', 
'tablet', 
'smart watch', 
'ear phone') not null;